﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace RecAndGenFakeObjs
{
    class PatternMatcher
    {
        private string word;
        private List<String> savedWords;
        private List<String> buisnessWords;

        public PatternMatcher(String word, List<String> savedWords, List<String> buisnessWords)
        {
            this.word = word;
            this.savedWords = savedWords;
            this.buisnessWords = buisnessWords;
        }

        public string findPattern()
        {
            String buildedAnswer = "";
            String savedWordsForRegex = buildWordsRegexList(savedWords);
            String buisnessWordsForRegex = buildWordsRegexList(buisnessWords);
            String coloredWords;
            char[] delimiterChars = { '.', '-', '_' };
            List<String> splittedWords = new List<String>();
            ColorWords cw = new ColorWords(savedWords, buisnessWords);

            splittedWords = word.Split(delimiterChars).ToList();

            foreach(String str in splittedWords)
            {
                coloredWords = cw.colorAllWords(str);
                String sep = findSeperator(word, str);
                buildedAnswer = buildedAnswer + coloredWords + sep;
                if(!word.Equals(str))
                    word = word.Substring(str.Length + 1);
            }

            return buildedAnswer;

            
        }

        private String buildWordsRegexList(List<String> words)
        {
            String ans = "";

            if (words.Count == 0)
                return ans;

            ans = words[0];
            for (int i = 1; i < words.Count; i++)
                ans += "|" + words[i];
 
            return ans;
        }

        private String findWord(String word, List<String> wordsList)
        {
            string result = wordsList.FirstOrDefault(x => x == word);

            return result;
        }

        private String findWordStartsWith(String word, List<String> wordsList)
        {
            string res = wordsList.FirstOrDefault(str => word.ToLower().StartsWith(str.ToLower()));

            return res;
        }

        private String findWordEndsWith(String word, List<String> wordsList)
        {
            string res = wordsList.FirstOrDefault(str => word.ToLower().EndsWith(str.ToLower()));

            return res;
        }

        public static String findSeperator(String word, String splitedWord)
        {
            String ans = "";
            if ((word.Length != 0) && (splitedWord.Length == 0))
                return word.Substring(0, 1);

            if (word.Length == 0)
                return "";

            ans = word.Replace(splitedWord, "");

            if (word.Equals(splitedWord))
                return  "";
            else
                return ans.Substring(0, 1);
        }
        
    }
}
