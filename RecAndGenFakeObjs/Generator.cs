﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace RecAndGenFakeObjs
{
    class Generator
    {
        private String pattern;
        private String name;
        private List<String> savedWords;
        private List<String> buisnessWords;
        private List<List<String>> namesDictionary;
        private List<String> swBig;
        private List<String> swSmall;
        private List<String> allWordsType = new string[] { "SW", "BW", "NM", "SN" }.ToList();
        private List<String> onlyBWandSW = new string[] { "SW", "BW" }.ToList();
        private List<String> allWords;

        public Generator(String pattern, String name, List<String> savedWords, List<String> buisnessWords, List<List<String>> namesDictionary, List<String> swbig, List<String> swsmall, List<String> allWords)
        {
            this.pattern = pattern;
            this.name = name;
            this.savedWords = savedWords;
            this.buisnessWords = buisnessWords;
            this.namesDictionary = namesDictionary;
            this.swBig = swbig;
            this.swSmall = swsmall;
            this.allWords = allWords;
        }

        public List<String> generateFakeObj()
        {
            char[] delimiterChars = { '.', '-', '_' };
            List<String> generatedWord = new List<String>();

            
            // Split the words with the patterns
            List<String> splittedPatternName = pattern.Split(delimiterChars).ToList();
            splittedPatternName.RemoveAll(str => String.IsNullOrEmpty(str)); 

            // Split all the words of the original word
            List<String> splittedName = name.Split(delimiterChars).ToList();
            splittedName.RemoveAll(str => String.IsNullOrEmpty(str));

            // allSWandBW will hold all the buisness words and the all the saved words
            List<String> allSWandBW = savedWords.Concat(buisnessWords).ToList();

            // String holding all the BW andd SW to use for the regexes 
            String allSWBWforRegex = ColorWords.buildWordsRegexList(allSWandBW);

            // If its only one word with pattern str and its lenght is 1, add 5 consecutive numbers in the end of the word
            if (pattern.Equals("str") && (name.Length == 1))
            {
                for (int j = 1; j < 6; j++)
                    generatedWord.Add(name + j.ToString());
            }
            // If its only one word with pattern str and its lenght is bigger than 1
            else if (pattern.Equals("str"))
            {
                generatedWord = replaceStrs(splittedPatternName, splittedName, name);
            }
            // If the word contains str pattern in each splitted word and we have more than one splitterd words replcate them
            else if (((splittedPatternName.Where(x => x.Contains("str")).ToList().Count == splittedPatternName.Count)) && (splittedPatternName.Count > 1))
            {
                generatedWord = replaceStrs(splittedPatternName, splittedName, name);
            }
            
            else if (pattern.EndsWith("str"))
            {
                generatedWord = replaceStrs(splittedPatternName, splittedName, name);
            }
            else if ((onlyBWandSW.Any(pattern.EndsWith)) && (!onlyBWandSW.Any(pattern.Equals)))
            {
                generatedWord = replaceSWorBW(@"(?!\s*$).+(" + allSWBWforRegex + @")\b", true);
            }
            else if (pattern.Contains("NM"))
            {
                generatedWord = replaceNums(name);
            }
            else if (onlyBWandSW.Any(pattern.StartsWith))
            {
                generatedWord = replaceSWorBW(@"\b(" + allSWBWforRegex + @")\w*", false);
            }

            if (pattern.Contains("SN"))
                generatedWord = replaceSN(name);

            //generatedWord.FirstOrDefault(str => str.ToLower().Equals(name.ToLower()))
            //String xxx = generatedWord.FirstOrDefault(str => str.ToLower().Equals(name.ToLower()));
            while (generatedWord.FirstOrDefault(str => str.ToLower().Equals(name.ToLower())) != null)
            {
                if (onlyBWandSW.Any(pattern.EndsWith))
                    generatedWord = replaceSWorBW(@"\w*(" + allSWBWforRegex + @")\b", true);
                else if (onlyBWandSW.Any(pattern.StartsWith))
                    generatedWord = replaceSWorBW(@"\b(" + allSWBWforRegex + @")\w*", false);
                else if (pattern.Contains("NM"))
                    generatedWord = replaceNums(name);
            }

            // check duplications
            var duplicateExists = generatedWord.GroupBy(n => n).Any(g => g.Count() > 1);
            if(duplicateExists)
                return null;

            HashSet<string> allWordsSet = new HashSet<string>(allWords);
            foreach (String fakeWord in generatedWord)
            {
                if (allWordsSet.Contains(fakeWord))
                    return null;
            }

            return generatedWord;
        }

        public string ToTitleCase(string str)
        {
            return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(str.ToLower());
        }


        private List<String> replaceSWorBW(String regexVal, bool isEnd)
        {
            Regex rgx;
            String SWBW = "";
            List<String> generatedWord = new List<string>();
            List<String> swDic;
            
            rgx = new Regex(regexVal, RegexOptions.IgnoreCase);
            SWBW = rgx.Match(name).Groups[1].Value;
            if (SWBW.Length > 4)
                swDic = new List<string>(swBig);
            else
                swDic = new List<string>(swSmall);



            int index = name.Sum(b => b) % savedWords.Count;
            int j = (index + 1) % swDic.Count;

            for (int k = 0; k < 5; k++ )
            {
                while (index != j)
                {
                    if (!name.ToLower().Contains(swDic[j]))
                    {
                        if (SWBW.All(c => char.IsUpper(c)))
                        {
                            if (isEnd)
                                generatedWord.Add(name.Substring(0, name.LastIndexOf(SWBW)) + swDic[j].ToUpper());
                            else
                                generatedWord.Add(swDic[j].ToUpper() + name.Substring(SWBW.Length));
                        }

                        else if (char.IsUpper(SWBW[0]))
                        {
                            if (isEnd)
                                generatedWord.Add(name.Substring(0, name.LastIndexOf(SWBW)) + ToTitleCase(swDic[j]));
                            else
                                generatedWord.Add(ToTitleCase(swDic[j]) + name.Substring(SWBW.Length));
                        }
                        else
                        {
                            if (isEnd)
                                generatedWord.Add(name.Substring(0, name.LastIndexOf(SWBW)) + swDic[j]);
                            else
                                generatedWord.Add(swDic[j] + name.Substring(SWBW.Length));
                        }
                        j = (j + 1 + k) % swDic.Count;
                        break;
                    }
                    j = (j + 1 + k) % swDic.Count;
                }
            }
            return generatedWord;
        }


        private List<String> replaceStrs(List<String> patterns, List<String> strs, String fullName)
        {
            List<String> generatedWord = new List<String>();

            for (int j = 0; j < 5; j++)
            {
                String tmpFullname = fullName.ToString();
                generatedWord.Add("");
                for (int i = 0; i < patterns.Count; i++)
                {
                    String sep = PatternMatcher.findSeperator(tmpFullname, strs[i]);
                    if (!patterns[i].Contains("str"))
                        generatedWord[j] = generatedWord[j] + strs[i] + sep;
                    else
                    {
                        if (strs[i].Length > 11)
                        {
                            if (strs[i].All(c => char.IsUpper(c)))
                                generatedWord[j] = generatedWord[j] + strs[i].ToUpper() + j.ToString() + sep;
                            else if (char.IsUpper(strs[i][0]))
                                generatedWord[j] = generatedWord[j] + ToTitleCase(strs[i]) + j.ToString() + sep;
                            else
                                generatedWord[j] = generatedWord[j] + strs[i].ToLower() + j.ToString() + sep;
                        }
                        else if (strs[i].Length < 2)
                        {
                            if (strs[i].All(c => char.IsUpper(c)))
                            {
                                char fake = ((char)(strs[i][0] + j + 1));
                                if (fake > 'z')
                                    fake = 'a';
                                else if (fake > 'Z')
                                    fake = 'A';

                                generatedWord[j] = generatedWord[j] + fake + sep;
                            }
                            
                        }
                        else
                        {
                            
                            int index = (strs[i].Sum(b => b) + j ) % namesDictionary[strs[i].Length - 2].Count;

                            while (strs[i].ToUpper().Equals(namesDictionary[strs[i].Length - 2][index].ToUpper()))
                                index = (index + 1) % namesDictionary[strs[i].Length - 2].Count;

                            if (strs[i].All(c => char.IsUpper(c)))
                                generatedWord[j] = generatedWord[j] + namesDictionary[strs[i].Length - 2][index].ToUpper() + sep;
                            else if (char.IsUpper(strs[i][0]))
                                generatedWord[j] = generatedWord[j] + ToTitleCase(namesDictionary[strs[i].Length - 2][index]) + sep;
                            else
                                generatedWord[j] = generatedWord[j] + namesDictionary[strs[i].Length - 2][index].ToLower() + sep;
                        }
                    }


                    if (!tmpFullname.Equals(strs[i]))
                        tmpFullname = tmpFullname.Substring(strs[i].Length + 1);
                }

            }
            return generatedWord;
        }

        private List<String> replaceStrsAtEnd(List<String> patterns, List<String> strs, String fullName)
        {
            List<String> generatedWord = new List<String>(5);

            for (int j = 0; j < 5; j++)
            {

                for (int i = 0; i < patterns.Count; i++)
                {
                    String sep = PatternMatcher.findSeperator(fullName, strs[i]);
                    if (i != (pattern.Length - 1))
                        generatedWord[j] = generatedWord[j] + strs[i] + sep;
                    else
                    {
                        if (strs[i].Length < 2 || strs[i].Length > 11)
                        {
                            if (strs[i].All(c => char.IsUpper(c)))
                                generatedWord[j] = generatedWord[j] + strs[i].ToUpper() + sep;
                            else if (char.IsUpper(strs[i][0]))
                                generatedWord[j] = generatedWord[j] + ToTitleCase(strs[i]) + sep;
                            else
                                generatedWord[j] = generatedWord[j] + strs[i].ToLower() + sep;
                        }
                        else
                        {
                            int index = strs[i].Sum(b => b) % namesDictionary[strs[i].Length - 2].Count;

                            if (strs[i].All(c => char.IsUpper(c)))
                                generatedWord[j] = generatedWord[j] + namesDictionary[strs[i].Length - 2][index].ToUpper() + sep;
                            else if (char.IsUpper(strs[i][0]))
                                generatedWord[j] = generatedWord[j] + ToTitleCase(namesDictionary[strs[i].Length - 2][index]) + sep;
                            else
                                generatedWord[j] = generatedWord[j] + namesDictionary[strs[i].Length - 2][index].ToLower() + sep;
                        }
                    }

                    if (!fullName.Equals(strs[i]))
                        fullName = fullName.Substring(strs[i].Length + 1);

                }
            }
            return generatedWord;
        }

        private List<String> replaceNums(String fullName)
        {
            List<String> generatedWord = new List<String>(5);
            Regex rgx;
            int index, len;
            String pattern = @"\d{1,2}", num = "";
            int fakeObjectsCreated = 0;
            int indexNum = 0;

            rgx = new Regex(pattern, RegexOptions.IgnoreCase);
            index = rgx.Match(fullName).Index;
            len = rgx.Match(fullName).Length;
            num = fullName.Substring(index, len);


            while (fakeObjectsCreated != 5)
            {
                String fakeWord = rgx.Replace(fullName, (Int32.Parse(num) + indexNum + 1).ToString(), 1);
                if (fakeWord.Length > 20)
                    fakeWord = fakeWord.Substring(1, fakeWord.Length - 1);

                indexNum++;
                if (allWords.Contains(fakeWord)) 
                    continue;
       
                generatedWord.Add(fakeWord);
                fakeObjectsCreated++;
            }

            return generatedWord;
        }

        private List<String> replaceSN(String fullName)
        {
            List<String> generatedWord = new List<String>(5);
            Regex rgx;
            int index, len;
            long bigNum;
            long internalBig;
            int fakeObjectsCreated = 0;
            int indexNum = 0;
            String pattern = @"\d{3,}", num = "";
            long sum = fullName.Sum(b => b);

            rgx = new Regex(pattern, RegexOptions.IgnoreCase);
            index = rgx.Match(fullName).Index;
            len = rgx.Match(fullName).Length;
            num = fullName.Substring(index, len);

            bigNum = long.Parse(num) + sum;
            internalBig = (long)Math.Pow(10, len);
            bigNum = bigNum % internalBig;


            while (fakeObjectsCreated != 5)
            {
                String fakeWord = rgx.Replace(fullName, (bigNum + indexNum).ToString(), 1);
                if (fakeWord.Length > 20)
                    fakeWord = fakeWord.Substring(1, fakeWord.Length - 1);

                indexNum++;

                if (allWords.Contains(fakeWord))
                    continue;

                generatedWord.Add(fakeWord);
                fakeObjectsCreated++;
            }

            return generatedWord;
        }
        
    }
}
