﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace RecAndGenFakeObjs
{
    class ColorWords
    {
        private List<String> savedWords;
        private List<String> buisnessWords;
        private List<String> wordsType = new string[] { "SW", "BW", "NM", "SN" }.ToList();

        public ColorWords(List<String> savedWords,List<String> buisnessWords)
        {
            this.savedWords = savedWords;
            this.buisnessWords = buisnessWords;
        }

        public String colorAllWords(String str)
        {
            String ans = "";
            ans = colorSW(str);
            if(buisnessWords.Count > 0)
                ans = colorBW(ans);
            ans = colorSN(ans);
            ans = colorNums(ans);
            ans = colorOthers(ans);

            return ans;
        }

        private String colorSW(String str)
        {
            String savedWordsForRegex = buildWordsRegexList(savedWords);
            string output = Regex.Replace(str, "(" + savedWordsForRegex + @")", "SW", RegexOptions.IgnoreCase);

            return output;
        }

        private String colorBW(String str)
        {
            String buisnessWordsForRegex = buildWordsRegexList(buisnessWords);
            string output = Regex.Replace(str, "(" + buisnessWordsForRegex + @")", "BW", RegexOptions.IgnoreCase);

            return output;

        }

        private String colorSN(String str)
        {
            String pattern = @"\d{3,}";

            string output = Regex.Replace(str, pattern, "SN", RegexOptions.IgnoreCase);

            return output;
        }

        private String colorNums(String str)
        {
            String pattern = @"\d{1,2}";

            string output = Regex.Replace(str, pattern, "NM", RegexOptions.IgnoreCase);

            return output;

        }

        private String colorOthers(String str)
        {

            string output = "";
            int notTypeWordindex = -1, nextTypeWordIndex = 0;
            notTypeWordindex = returnFirstIndexThatTypeWordNotExist(str);

            if (str.Length == notTypeWordindex)
                return str;
            else
                output += str.Substring(0, notTypeWordindex);

            while (str.Length != notTypeWordindex)
            {
                output += "str";
                nextTypeWordIndex = returnNextIndexOfTypeWord(str, notTypeWordindex);
                if (nextTypeWordIndex != -1)
                {
                    str = str.Substring(nextTypeWordIndex);
                    notTypeWordindex = returnFirstIndexThatTypeWordNotExist(str);
                    output += str.Substring(0, notTypeWordindex);
                }
                else
                    break;
            }

            return output;
        }
        
        public static String buildWordsRegexList(List<String> words)
        {
            String ans = "";

            if (words.Count == 0)
                return ans;

            ans = words[0];
            for (int i = 1; i < words.Count; i++)
                ans += "|" + words[i];

            return ans;
        }

        private int returnFirstIndexThatTypeWordNotExist(String str)
        {
            
            int index = 0;

            while (wordsType.Any(str.StartsWith))
            {
                str = str.Substring(2);
                index += 2;
            }
      
            return index;
        }

        private int returnNextIndexOfTypeWord(String str, int notTypeWordindex)
        {
            String tmpString;
            int index = -1;
            int smallestIndex=1000;

            tmpString = str.Substring(notTypeWordindex);

            foreach (String type in wordsType)
            {
                index = tmpString.IndexOf(type);
                if (index != -1)
                {
                    if (index < smallestIndex)
                        smallestIndex = index;
                }
            }

            if (smallestIndex != 1000)
                return notTypeWordindex + smallestIndex;
            else
                return -1; 
            
        }
        
    }
}
