﻿using FindMostCommonPattern;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecAndGenFakeObjs
{
    class Program
    {
        static String kiwidir;
        static String filterWordsFilePath;
        static String buisnessWordFilePath;
        static String inputFileToFakePath;
        static String allDomainUsers;
        static String deceptiveUser;
        static List<String> savedWords;
        static List<List<String>> namesDictionaryByLength;
        static bool isUsers;
        static bool isCalcBW;
        static int MaximumStringArrayInC = 16380;

        static void Main(string[] args)
        {

            if (!isValidInput(args))
                return;

            fillAllGlobalMemebers(args);

           
            // get all the saved words and all the dictionaries to filter those words from all the objects list and remain with the BW
            List<String> filterWords = createBigDic();
            

            // Recalculate the business words
            if (isCalcBW)
            {
                calculateBW(filterWords);
            }
            // Generate all the Fake Object for all the objects in the arg[1] input file
            else
            {                
                generateFakeObjectsFiles();
            }

            File.Delete(kiwidir + "\\genFake\\SWbig.txt");
            File.Delete(kiwidir + "\\genFake\\SWsmall.txt");
                
        }

        public static List<List<String>> createDicLengths()
        {
            List<String> len2 = File.ReadAllLines(kiwidir + "\\genFake\\NamesDictionary\\len2.txt").ToList();
            List<String> len3 = File.ReadAllLines(kiwidir + "\\genFake\\NamesDictionary\\len3.txt").ToList();
            List<String> len4 = File.ReadAllLines(kiwidir + "\\genFake\\NamesDictionary\\len4.txt").ToList();
            List<String> len5 = File.ReadAllLines(kiwidir + "\\genFake\\NamesDictionary\\len5.txt").ToList();
            List<String> len6 = File.ReadAllLines(kiwidir + "\\genFake\\NamesDictionary\\len6.txt").ToList();
            List<String> len7 = File.ReadAllLines(kiwidir + "\\genFake\\NamesDictionary\\len7.txt").ToList();
            List<String> len8 = File.ReadAllLines(kiwidir + "\\genFake\\NamesDictionary\\len8.txt").ToList();
            List<String> len9 = File.ReadAllLines(kiwidir + "\\genFake\\NamesDictionary\\len9.txt").ToList();
            List<String> len10 = File.ReadAllLines(kiwidir + "\\genFake\\NamesDictionary\\len10.txt").ToList();
            List<String> len11 = File.ReadAllLines(kiwidir + "\\genFake\\NamesDictionary\\len11.txt").ToList();

            List<List<String>> namesDictionary = new List<List<string>>();
            namesDictionary.Add(len2);
            namesDictionary.Add(len3);
            namesDictionary.Add(len4);
            namesDictionary.Add(len5);
            namesDictionary.Add(len6);
            namesDictionary.Add(len7);
            namesDictionary.Add(len8);
            namesDictionary.Add(len9);
            namesDictionary.Add(len10);
            namesDictionary.Add(len11);

            return namesDictionary;
        }

        public static List<String> createBigDic()
        {
            List<String> names = new List<String>();
            foreach (List<String> l in namesDictionaryByLength)
                names = names.Concat(l).ToList();

            List<String> isrFemaleWords = File.ReadAllLines(kiwidir + "\\genFake\\NamesDictionary\\israel_female.txt").ToList();
            List<String> isrMaleWords = File.ReadAllLines(kiwidir + "\\genFake\\NamesDictionary\\israel_male.txt").ToList();
            List<String> freWords = File.ReadAllLines(kiwidir + "\\genFake\\NamesDictionary\\french_names.txt").ToList();
            List<String> spaWords = File.ReadAllLines(kiwidir + "\\genFake\\NamesDictionary\\spanish_names.txt").ToList();

            System.IO.StreamWriter swSmall = new System.IO.StreamWriter(kiwidir + "\\genFake\\SWsmall.txt");
            System.IO.StreamWriter swBig = new System.IO.StreamWriter(kiwidir + "\\genFake\\SWbig.txt");
            foreach (String str in savedWords)
            {
                if (str.Length > 4)
                    swBig.WriteLine(str);
                else
                    swSmall.WriteLine(str);
            }
            swBig.Close();
            swSmall.Close();
             
            List<String> filterWords = new List<string>();
            filterWords = savedWords.Concat(names).ToList();
            filterWords = filterWords.Concat(isrFemaleWords).ToList();
            filterWords = filterWords.Concat(isrMaleWords).ToList();
            filterWords = filterWords.Concat(freWords).ToList();
            filterWords = filterWords.Concat(spaWords).ToList();

            filterWords = filterWords.ConvertAll(d => d.ToLower());

            return filterWords;
        }

        public static bool isValidInput(string[] args)
        {
            // Make sure we have enough arguments
            if (args.Length < 4)
            {
                System.Console.WriteLine("Correct usage:");
                System.Console.WriteLine("ADObjectFilter <kiwi_path> <file_to_read> <1/0 (find_BW/Generate_fake_objects)> <1/0 (Users/Computer)>");

                return false;
            }
            else if ((args[3].Equals("1")) && (args[2].Equals("0")))
            {
                if (args.Length < 5)
                {
                    System.Console.WriteLine("When working with the users you must add last argument containing all the domain users list");
                    return false;
                }
                else if (!File.Exists(args[4]))
                {
                    System.Console.WriteLine("Unvalid file: " + args[4]);
                    return false;
                }

            }

            try
            {
                Path.GetDirectoryName(args[0]);
            }
            catch
            {
                System.Console.WriteLine("Unvalid path: " + args[0]);
                return false;
            }

            if (!File.Exists(args[1]))
            {
                System.Console.WriteLine("Unvalid file: " + args[1]);
                return false;
            }

            if((!args[2].Equals("1")) && (!args[2].Equals("0")))
            {
                System.Console.WriteLine("Parameter 3 must be 1/0, got " + args[2]);
                return false;
            }

            if ((!args[3].Equals("1")) && (!args[3].Equals("0")))
            {
                System.Console.WriteLine("Parameter 4 must be 1/0, got " + args[3]);
                return false;
            }

            return true;
        }

        static void fillAllGlobalMemebers(string[] args)
        {
            kiwidir = args[0];
            inputFileToFakePath = args[1];
            isCalcBW = args[2].Equals("1");
            filterWordsFilePath = kiwidir + "\\genFake\\filtered.txt";
            savedWords = File.ReadAllLines(kiwidir + "\\genFake\\SW.txt").ToList();

            // create list of dictionaries with different lenghts from 2-11
            namesDictionaryByLength = createDicLengths();

            // Choose if we want to relate the users file or the computers file
            if (args[3].Equals("1"))
            {
                isUsers = true;
                buisnessWordFilePath = kiwidir + "\\genFake\\usersBW.txt";
                if (args[2].Equals("0"))
                {
                    allDomainUsers = args[4];
                    deceptiveUser = args[5];
                }
            }
            else
            {
                isUsers = false;
                buisnessWordFilePath = kiwidir + "\\genFake\\computersBW.txt";
            }
        }

        static void calculateBW(List<String> filterWords)
        {
            CSVHandler csvHandler;
            csvHandler = new CSVHandler(inputFileToFakePath, filterWordsFilePath);
            if (isUsers)
                csvHandler.createFilteredCSV_DomainUsers();
            else
                csvHandler.createFilteredCSV_DomainComputersNoSW(savedWords);

            csvHandler.closeHandles();

            // Get the Buisness Words from the domain users
            BuisnessWordsFilter bwf = new BuisnessWordsFilter(filterWordsFilePath, buisnessWordFilePath, isUsers);
            bwf.findBuisnessWords(filterWords);
            File.Delete(filterWordsFilePath);
            return;
        }

        static void generateFakeObjectsFiles()
        {
            // Handle to the relevent .h file (users, computers)
            System.IO.StreamWriter hFile;

            // Handle to the netObjs.txt file
            System.IO.StreamWriter fakeFile;

            StringBuilder realAndFakes = new StringBuilder();
            String compByte = "", numOfFakeObjectsString="";
            int index;

            Stream inStream, outStream; 

            List<String> allWords;
            List<String> allFakes = new List<string>();
            List<String> buisnessWords = File.ReadAllLines(buisnessWordFilePath).ToList();
            List<String> words = File.ReadAllLines(inputFileToFakePath).ToList();
            List<String> swbig = File.ReadAllLines(kiwidir + "\\genFake\\SWbig.txt").ToList();
            List<String> swsmall = File.ReadAllLines(kiwidir + "\\genFake\\SWsmall.txt").ToList();
            Array.Sort(words.ToArray(), StringComparer.OrdinalIgnoreCase);

            if (isUsers)
            {
                hFile = new System.IO.StreamWriter(kiwidir + "\\genFake\\netUsers.h");
                fakeFile = new System.IO.StreamWriter(kiwidir + "\\netObjs.txt");
                allWords = File.ReadAllLines(allDomainUsers).ToList();
            }
            else
            {
                hFile = new System.IO.StreamWriter(kiwidir + "\\genFake\\netComputers.h");
                fakeFile = new System.IO.StreamWriter(kiwidir + "\\netObjs.txt", true);
                allWords = File.ReadAllLines(inputFileToFakePath).ToList();
            }

            hFile.WriteLine("#include \"netData.h\"\n");

            if (isUsers)
            {
                hFile.WriteLine("#define FAKE_OBJS_NUM 5\n");
                hFile.Write("char* usersTuples[] = {");
            }
            else
                hFile.Write("char* computersTuples[] = {");

            

            if (!isUsers)
                compByte = "$";

            foreach (String word in words)
            {
                Boolean found = false;
                PatternMatcher patternMatcher = new PatternMatcher(word.ToLower(), savedWords, buisnessWords);
                String pat = patternMatcher.findPattern();

                Generator gen = new Generator(pat, word, savedWords, buisnessWords, namesDictionaryByLength, swbig, swsmall, allWords);
                List<String> fakes = gen.generateFakeObj();

                if (fakes == null || fakes.Count == 0)
                    continue;
                
                foreach (String fake in fakes)
                {
                    List<String> tmpFakes = new List<string>(allFakes);
                    tmpFakes = tmpFakes.ConvertAll(d => d.ToLower());
                    if (tmpFakes.Any(fake.ToLower().Contains))
                        found = true;
                }

                if (found)
                    continue;
                
                allFakes.AddRange(fakes);

                
                realAndFakes.Append("," + word + compByte);

                for (int i = 0; i < 5; i++)
                {
                    realAndFakes.Append("," + fakes[i] + compByte);
                    if (words[words.Count - 1].Equals(word) && i == 4)
                    {
                        if (!isUsers)
                            fakeFile.Write(fakes[i] + compByte);
                        else
                            fakeFile.WriteLine(fakes[i] + compByte);
                    }   
                    else
                        fakeFile.WriteLine(fakes[i] + compByte);
                }
            }
            realAndFakes.Append(",");
            var bytesToCompress = Encoding.UTF8.GetBytes(realAndFakes.ToString());
            var compressedData = new byte[realAndFakes.Length * 2];

            inStream = new MemoryStream(bytesToCompress); // compres in-memory byte array
            outStream = new MemoryStream(compressedData); // to file stream
           
            SevenZip.Helper.Compress(inStream, outStream);
            String myStr = ByteArrayToHexString(compressedData, outStream.Position);

            for (index = 0; index < myStr.Length; index += MaximumStringArrayInC)
            {
                if ((myStr.Length - index) < MaximumStringArrayInC)
                    break;

                hFile.WriteLine("\"" + myStr.Substring(index, MaximumStringArrayInC) + "\",");
            }

            hFile.WriteLine("\"" + myStr.Substring(index) + "\"};");

            if (isUsers)
            {
                hFile.Write("char* fakeUsers[] = {");
                numOfFakeObjectsString = "numOfFakeUsers";
            }
            else
            {
                hFile.Write("char* fakeComputers[] = {");
                numOfFakeObjectsString = "numOfFakeComputers";
            }

            inStream.Flush();
            outStream.Flush();
            //allFakes = allFakes.ConvertAll(d => d.ToLower());
            string[] testArray = allFakes.ToArray();            
            Array.Sort(testArray, StringComparer.Ordinal);
            List<String> orderedArray = testArray.ToList<String>();

            StringBuilder fakeObjects = new StringBuilder();
            for (int i = 0; i < orderedArray.Count; i++)
                fakeObjects.Append("," + orderedArray[i] + compByte);

            fakeObjects.Append(",");

            bytesToCompress = Encoding.UTF8.GetBytes(fakeObjects.ToString());
            compressedData = new byte[fakeObjects.Length * 2];

            inStream = new MemoryStream(bytesToCompress); // compres in-memory byte array
            outStream = new MemoryStream(compressedData);// to file stream

            SevenZip.Helper.Compress(inStream, outStream);
            myStr = ByteArrayToHexString(compressedData, outStream.Position);


            for (index = 0; index < myStr.Length; index += MaximumStringArrayInC)
            {
                if (myStr.Length - index < MaximumStringArrayInC)
                    break;

                hFile.WriteLine("\"" + myStr.Substring(index, MaximumStringArrayInC) + "\",");
            }

            hFile.WriteLine("\"" + myStr.Substring(index) + "\"};");
            hFile.WriteLine("int " + numOfFakeObjectsString + "=" + orderedArray.Count + ";");
            if (isUsers)
            {
                hFile.WriteLine("WCHAR* deceptiveUser[] = {L\"" + deceptiveUser + "\"};");
            }
            
            fakeFile.Close();
            hFile.Close();
        }

        static byte[] GetBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }

        public static string ByteArrayToHexString(byte[] ba, long length)
        {
            string hex = BitConverter.ToString(ba, 0, (int)length);
            return hex.Replace("-", "");
        }
    }
}
