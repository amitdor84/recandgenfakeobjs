﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace RecAndGenFakeObjs
{
    class BuisnessWordsFilter 
    {
        private String inputFile;
        private String buisnessWordsFilePath;
        private bool isUsers;

        public BuisnessWordsFilter(String inFile, String buisnessWordsFilePath, bool isUsers)
        {
            this.inputFile = inFile;
            this.buisnessWordsFilePath = buisnessWordsFilePath;
            this.isUsers = isUsers;
        }

        public void findBuisnessWords(List<String> savedWords)
        {
            List<string> strList = new List<string>();
            var dict = new Dictionary<string, int>();
            var dictNoDup = new Dictionary<string, int>();
            int threshhold;

            if (isUsers)
                threshhold = 3;
            else
                threshhold = 30;

            dict = getDictionaryOfWords(savedWords);

            // Get ordered list of the dictionary by words occurences
            var ordered = from k in dict.Keys
                          orderby dict[k] descending
                          select k;

            // Write the buisness words to file with their repeatations
            using (StreamWriter output = new StreamWriter(buisnessWordsFilePath))
            {
                foreach (String k in ordered)
                {
                    String lower = k.ToLower();
                    if (dictNoDup.ContainsKey(lower))
                        continue;
                    else
                        dictNoDup[lower] = dict[k];
                }
                
                foreach (String k in ordered)
                {
                    if (dict[k] > threshhold && k.Length > 2)
                    {
                        if(dict[k] == dictNoDup[k.ToLower()])
                            output.WriteLine(String.Format("{0}", k, dict[k]));
                    }
                        
                }
                output.Close();
            }
        }


        private List<String> findAllSubstring(String str, List<String> savedWords)
        {
            List<String> temp = new List<String>();
            List<String> ans = new List<String>();
            char[] delimiterChars = { '.', '-', '_' };

            temp = str.Split(delimiterChars).ToList();
            for (int i = 0; i < temp.Count; i++)
            {
                var result = Regex.Match(temp[i], @"\d+").Value;
                while (result.Length != 0)
                {
                    temp[i] = temp[i].Replace(result.ToString(), "");
                    result = Regex.Match(temp[i], @"\d+").Value;
                }

                if (isUsers)
                {
                    if (!savedWords.Any(temp[i].ToLower().Contains))
                        ans.Add(temp[i]);
                }
                else
                {
                    if (!savedWords.Any(temp[i].ToLower().Equals))
                        ans.Add(temp[i]);
                }
            }

            return ans;
        }

        private Dictionary<string, int> getDictionaryOfWords(List<String> savedWords)
        {
            String line;
            List<String> substringsCollection;
            var dict = new Dictionary<string, int>();

            System.IO.StreamReader file = new System.IO.StreamReader(this.inputFile);

            while ((line = file.ReadLine()) != null)
            {
                var result = Regex.Split(line, @"\s+");
                foreach (String name in result)
                {
                    if (name.Length == 0)
                        continue;

                    substringsCollection = findAllSubstring(name, savedWords);
                    foreach (String word in substringsCollection)
                    {
                        if (dict.ContainsKey(word))
                            dict[word]++;
                        else
                            dict[word] = 1;
                    }
                }
            }

            file.Close();

            return dict;
        }

        private List<String> getWordsWithoutThierPrefixes(IOrderedEnumerable<String> ordered, Dictionary<string, int> dict)
        {
            List<string> strList = new List<string>();
            bool found;
            foreach (String k in ordered)
            {
                found = false;
                for (int i = 0; i < strList.Count; i++)
                {
                    if (strList[i].Contains(k))
                    {
                        found = true;
                        break;
                    }
                    if (k.Contains(strList[i]))// && (dict[k] == dict[strList[i]]))
                    {
                        found = true;
                        strList[i] = k;
                        break;
                    }
                }
                if (!found)
                    strList.Add(k);
            }

            return strList;
        }
    }
}
