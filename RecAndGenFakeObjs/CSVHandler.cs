﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using ReadWriteCsv;

namespace FindMostCommonPattern
{
    class CSVHandler
    {
        CsvFileReader reader = null;
        CsvFileWriter writer = null;

        public CSVHandler(String csv_filename_to_read, String csv_filename_to_write)
        {
            writer = new CsvFileWriter(csv_filename_to_write);
            reader = new CsvFileReader(csv_filename_to_read);
        }

        public String findSubstringWithLength(String str, int len)
        {
            if (str.Length >= len)
                return str.Substring(0, len);
            else
                return "";
        }

        public void SetCSV_Write(String csv_filename_to_write)
        {
            // Write sample data to CSV file
            writer = new CsvFileWriter(csv_filename_to_write);
        }

        public void SetCSV_Read(String csv_filename_to_read)
        {
            // Read sample data from CSV file
            reader = new CsvFileReader(csv_filename_to_read);
        }



        public void createFilteredCSV_DomainUsers()
        {
            resetPointers();
            CsvRow row = new CsvRow();
            
            while (reader.ReadRow(row))
            {
                CsvRow newRow = new CsvRow();
                newRow.Add(row[0]);
                writer.WriteRow(newRow);
            }
        }


        public void createFilteredCSV_DomainComputersNoSW(List<String> saved_words)
        {
            resetPointers();
            CsvRow row = new CsvRow();

            while (reader.ReadRow(row))
            {
                CsvRow newRow = new CsvRow();

                row[0] = row[0].Replace("$", "");
                if (saved_words.Any(row[0].ToLower().Contains))
                {
                    foreach (string str in saved_words)
                    {
                        if (row[0].ToLower().Contains(str))
                        {
                            row[0] = row[0].ToLower().Replace(str, "");
                        }
                    }
                 /*   
                    while (row[0].StartsWith("-"))
                        row[0] = row[0].Substring(1);

                    while (row[0].StartsWith("."))
                        row[0] = row[0].Substring(1);

                    while (row[0].StartsWith("_"))
                        row[0] = row[0].Substring(1);
                    */
                    if (row[0].Length != 0)
                    {
                        newRow.Add(row[0]);
                        writer.WriteRow(newRow);
                    }
                    continue;
                }

                else
                {
                    newRow.Add(row[0]);
                    writer.WriteRow(newRow);
                }
            }
        }

        public void createFilteredCSV_DomainComputers()
        {
            resetPointers();
            CsvRow row = new CsvRow();

            while (reader.ReadRow(row))
            {
                CsvRow newRow = new CsvRow();
                if (!(row[2].Equals("Domain Computers")))
                    continue;

                row[0] = row[0].Replace("$", "");
                newRow.Add(row[0]);
                writer.WriteRow(newRow);
                
            }
        }

        /*public void createCSV_newColumn()
        {
            resetPointers();
            CsvRow row = new CsvRow();
            String prefix;
            var dict = new Dictionary<string, int>();
            int occurrences;
            while (reader.ReadRow(row))
            {
                prefix = findSubstringWithLength(row[0], 3);

                if (prefix.Length != 0)
                {
                    if (dict.ContainsKey(prefix))
                    {
                        dict[prefix]++;
                    }
                    else
                    {
                        dict[prefix] = 1;
                    }
                }
                
            }

            reader.DiscardBufferedData();
            reader.BaseStream.Seek(0, System.IO.SeekOrigin.Begin);
            reader.ReadRow(row);
            row.Add("First 3 characters occurences");
            writer.WriteRow(row);

            while (reader.ReadRow(row))
            {
                prefix = findSubstringWithLength(row[0], 3);
                if (prefix.Length != 0)
                {
                    occurrences = dict[prefix];
                    row.Add("" + occurrences);
                    writer.WriteRow(row);
                }
            }
        }*/

        private void resetPointers()
        {
            reader.DiscardBufferedData();
            reader.BaseStream.Seek(0, System.IO.SeekOrigin.Begin);
            writer.BaseStream.Seek(0, System.IO.SeekOrigin.Begin);
        }

        public void closeHandles()
        {
            reader.Close();
            writer.Close();
        }
    }
}
